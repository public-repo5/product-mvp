# product-mvp

#### System Overview

![System Overview](./docs/system.png)

Product MVP was built on top of SpringCloud, API Swagger, SQL Dabatase.
- BFF Service: (Backend for FrontEnd) recevie requests from end users, calls into internal services, then responds the result. (Bff service was skipped in source code)
- Product Service: Restful service for creating, searching products.
- Cart Service: Restful service for creating, adding, getting cart information.
- H2 Database: Memmory database for each micro-service (for MVP version only), we can change to any database such as postgresql, mysql,.. by updating database connection url in configuration file.

#### UML

![Product & Cart Entity](./docs/models.png)

#### Sequence Diagrams

![Create Product](./docs/product.png)
![Add to Cart](./docs/cart.png)

### Getting Started

1. Install docker https://docs.docker.com/get-docker/
2. Install docker-compose https://docs.docker.com/compose/install/
3. Install maven http://maven.apache.org/install.html
4. Run run.sh file

### Testing with cURL

###### Create Product

`curl --location --request POST 'http://localhost:8080/product' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
  "name": "m",
  "code": "do amet",
  "category": "category",
  "brand": "brand",
  "color": "color",
  "currentPrice": 1000,
  "imageURL": "culpa magna"
}'`

###### Search Product

`curl --location --request GET 'http://localhost:8080/products?category=category&fromPrice=0&toPrice=999999999999&brand=brand&color=color&offset=0&limit=2' \
--header 'Accept: application/json'`

##### Add product to an empty cart
`curl --location --request POST 'http://localhost:9090/cart' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
  "productId": 1,
  "quantity": 3
}'`
##### Add product to an existing cart
`curl --location --request POST 'http://localhost:9090/cart' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
 "id":1,
  "productId": 2,
  "quantity": 4
}'`

#### Have fun!

