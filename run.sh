#!/bin/bash
docker-compose down
cd product-service
mvn clean install
cd ../cart-service
mvn clean install
cd ../
docker-compose build
docker-compose up
