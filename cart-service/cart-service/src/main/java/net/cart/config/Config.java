package net.cart.config;

import net.cart.mock.MockProductApi;
import net.product.api.ProductApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {
    @Bean
    @Profile("!unittest")
    ProductApi productApi() {
        return new ProductApi();
    }

    @Bean
    @Profile("unittest")
    ProductApi mockProductApi() {
        return new MockProductApi();
    }
}
