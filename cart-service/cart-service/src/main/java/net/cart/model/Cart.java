/**
 *
 */

package net.cart.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author namho
 * 
 */
@Getter
@Setter
@Entity
@Table(name = "CART")
@AttributeOverride(name = "id", column = @Column(name = "ID", insertable = false, updatable = false))
@GenericGenerator(name = "SEQ_GEN", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
  parameters = { @Parameter(name = "sequence_name", value = "SEQ_CART") })
public class Cart {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
  private Long id;

  @OneToMany
  @JoinColumn(name = "cart_id")
  private List<CartItem> items;

  @Column(name = "createdBy")
  private String createdBy;

  @Column(name = "createdAt")
  private Long createdAt;

  @Column(name = "updatedBy")
  private String updatedBy;

  @Column(name = "updatedAt")
  private Long updatedAt;

  @PrePersist
  public void onCreate() {
    this.createdAt = System.currentTimeMillis();
  }
}
