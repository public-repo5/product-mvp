package net.cart.mock;

import net.product.ProductResponse;
import net.product.api.ProductApi;
import org.springframework.web.client.RestClientException;

import java.math.BigDecimal;

public class MockProductApi extends ProductApi {
    @Override
    public ProductResponse getProduct(Long id) throws RestClientException {
        ProductResponse mockProduct = new ProductResponse();
        mockProduct.setId(1l);
        mockProduct.category("E-DEVICE");
        mockProduct.code("IPHONE12");
        mockProduct.currentPrice(BigDecimal.valueOf(1000));
        mockProduct.imageURL("http://iphone12.com.vn");
        mockProduct.category("category");
        mockProduct.brand("brand");
        mockProduct.color("color");
        mockProduct.name("iPhone 12");
        return mockProduct;
    }
}
