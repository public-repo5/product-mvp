/**
 *
 */

package net.cart.repo;

import net.cart.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author namho
 * 
 */
@Repository
public interface CartRepo extends JpaRepository<Cart, Long> {

}
