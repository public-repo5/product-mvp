/**
 *
 */

package net.cart.repo;

import net.cart.model.Cart;
import net.cart.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author namho
 * 
 */
@Repository
public interface CartItemRepo extends JpaRepository<CartItem, Long> {

}
