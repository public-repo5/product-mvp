/**
 *
 */

package net.cart.mapper;

import net.cart.model.Cart;
import net.cart.model.CartItem;
import net.cart.model.CartResponse;
import net.product.ProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author namho
 * 
 */
@Mapper
public interface CartMapper {
  CartMapper INSTANCE = Mappers.getMapper(CartMapper.class);
  default CartItem toCartItem(ProductResponse product){
    CartItem cartItem = new CartItem();
    cartItem.setCategory(product.getCategory());
    cartItem.setBrand(product.getBrand());
    cartItem.setColor(product.getColor());
    cartItem.setCurrentPrice(product.getCurrentPrice());
    cartItem.setProductId(product.getId());
    cartItem.setImageURL(product.getImageURL());
    cartItem.setName(product.getName());
    return cartItem;
  }
  CartResponse toCartResponse(Cart cart);
}
