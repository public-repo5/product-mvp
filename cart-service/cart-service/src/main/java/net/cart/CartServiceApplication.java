package net.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCircuitBreaker
@ComponentScan(basePackageClasses = { CartServiceApplication.class })
@EntityScan(basePackageClasses = CartServiceApplication.class)
public class CartServiceApplication {

  public static void main(final String[] args) {
    SpringApplication.run(CartServiceApplication.class, args);
  }
}
