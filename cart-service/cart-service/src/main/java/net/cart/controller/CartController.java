/**
 *
 */

package net.cart.controller;

import lombok.extern.slf4j.Slf4j;
import net.cart.mapper.CartMapper;
import net.cart.model.Cart;
import net.cart.model.CartPostRequest;
import net.cart.model.CartResponse;
import net.cart.service.CartService;
import org.openapitools.api.CartApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

/**
 * @author namho
 * 
 */
@Controller
@Slf4j
public class CartController implements CartApi {
    private CartService cartService;

    public CartController(@Autowired CartService cartService) {
        this.cartService = cartService;
    }

    @Override
    public ResponseEntity<CartResponse> addToCart(CartPostRequest cartPostRequest) {
        Cart cart = cartService.addToCart(cartPostRequest);
        if (cart == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(CartMapper.INSTANCE.toCartResponse(cart));
    }

    @Override
    public ResponseEntity<CartResponse> getCart(Long id) {
        return cartService.getCart(id).map(cart -> ResponseEntity.ok(CartMapper.INSTANCE
                .toCartResponse(cart))).orElse(ResponseEntity.notFound().build());
    }
}
