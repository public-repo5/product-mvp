/**
 *
 */

package net.cart.service;

import lombok.extern.slf4j.Slf4j;
import net.cart.mapper.CartMapper;
import net.cart.model.Cart;
import net.cart.model.CartItem;
import net.cart.model.CartPostRequest;
import net.cart.repo.CartItemRepo;
import net.cart.repo.CartRepo;
import net.product.ProductResponse;
import net.product.api.ProductApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @author namho
 *
 */
@Slf4j
@Service
public class CartService {

    private CartRepo cartRepo;
    private CartItemRepo cartItemRepo;
    private ProductApi productApi;

    public CartService(@Autowired CartRepo cartRepo, @Autowired CartItemRepo cartItemRepo, @Autowired ProductApi productApi) {
        this.cartRepo = cartRepo;
        this.cartItemRepo =cartItemRepo;
        this.productApi = productApi;
    }


    @Transactional
    public Cart addToCart(CartPostRequest cartPostRequest) {
        Cart cart;
        if (cartPostRequest.getId() == null) {
            cart = new Cart();
            cart.setItems(new ArrayList<>());
            cart = cartRepo.save(cart);
        } else {
            cart = cartRepo.findById(cartPostRequest.getId()).get();
        }
        if (cart == null) {
            return null;
        }
        ProductResponse productResponse = this.productApi.getProduct(cartPostRequest.getProductId());
        if (productResponse == null) {
            return null;
        }
        CartItem item = CartMapper.INSTANCE.toCartItem(productResponse);
        item.setQuantity(cartPostRequest.getQuantity());
        cart.getItems().add(item);
        cartItemRepo.save(item);
        return cart;
    }

    public Optional<Cart> getCart(Long id) {
        return cartRepo.findById(id);
    }
}
