package net.cart.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.cart.model.CartPostRequest;
import net.product.ProductResponse;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 */
public class CartControllerTest extends AbstractApiTest {

    @Test
    public void addToCart() throws Exception {
        ProductResponse mockProduct = new ProductResponse();
        mockProduct.setId(1l);
        mockProduct.category("E-DEVICE");
        mockProduct.code("IPHONE12");
        mockProduct.currentPrice(BigDecimal.valueOf(1000));
        mockProduct.imageURL("http://iphone12.com.vn");
        mockProduct.category("category");
        mockProduct.brand("brand");
        mockProduct.color("color");
        mockProduct.name("iPhone 12");
        final CartPostRequest cart = new CartPostRequest();
        cart.setQuantity(1);
        cart.setProductId(1l);
        mockMvc.perform(post("/cart")
                        .content(new ObjectMapper().writeValueAsString(cart)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].productId").value(1l))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].quantity").value(1l))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].category").value(mockProduct.getCategory()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].currentPrice").value(mockProduct.getCurrentPrice()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].brand").value(mockProduct.getBrand()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].color").value(mockProduct.getColor()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].name").value(mockProduct.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[0].imageURL").value(mockProduct.getImageURL()));
        cart.setQuantity(2);
        cart.setId(1l);
        cart.setProductId(1l);
        mockMvc.perform(post("/cart")
                        .content(new ObjectMapper().writeValueAsString(cart)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.items[1].quantity").value(2l));

    }
}