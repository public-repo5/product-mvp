package net.product.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import net.product.model.ProductPostRequest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 */
public class ProductControllerTest extends AbstractApiTest {

    @Test
    public void createProduct() throws Exception {
        final ProductPostRequest productPostRequest = new ProductPostRequest();
        productPostRequest.category("E-DEVICE");
        productPostRequest.code("IPHONE12");
        productPostRequest.currentPrice(BigDecimal.valueOf(1000));
        productPostRequest.imageURL("http://iphone12.com.vn");
        productPostRequest.category("category");
        productPostRequest.brand("brand");
        productPostRequest.color("color");
        productPostRequest.name("iPhone 12");
        mockMvc.perform(post("/product")
                        .content(new ObjectMapper().writeValueAsString(productPostRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void listProduct() throws Exception {
        final ProductPostRequest productPostRequest = new ProductPostRequest();
        productPostRequest.category("E-DEVICE");
        productPostRequest.code("IPHONE12");
        productPostRequest.currentPrice(BigDecimal.valueOf(1000));
        productPostRequest.imageURL("http://iphone12.com.vn");
        productPostRequest.category("category");
        productPostRequest.brand("brand");
        productPostRequest.color("color");
        productPostRequest.name("iPhone 12");
        mockMvc.perform(post("/product")
                        .content(new ObjectMapper().writeValueAsString(productPostRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(get("/products?category=category").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.products[*].category").value("category"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.total").value(1l));
        mockMvc.perform(get("/products?category=categoryxxxx").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.products").isEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.total").value(0l));
    }

    @Test
    public void getProduct() throws Exception {
        final ProductPostRequest productPostRequest = new ProductPostRequest();
        productPostRequest.category("E-DEVICE");
        productPostRequest.code("IPHONE12");
        productPostRequest.currentPrice(BigDecimal.valueOf(1000));
        productPostRequest.imageURL("http://iphone12.com.vn");
        productPostRequest.category("category");
        productPostRequest.brand("brand");
        productPostRequest.color("color");
        productPostRequest.name("iPhone 12");
        MvcResult mvcResult = mockMvc.perform(post("/product")
                        .content(new ObjectMapper().writeValueAsString(productPostRequest)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.category").value(productPostRequest.getCategory()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(productPostRequest.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.currentPrice").value(productPostRequest.getCurrentPrice()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brand").value(productPostRequest.getBrand()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.color").value(productPostRequest.getColor()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(productPostRequest.getName()))
                .andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        Integer id = JsonPath.parse(response).read("$.id");
        mockMvc.perform(get("/product/" + id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(get("/product/212343545").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}