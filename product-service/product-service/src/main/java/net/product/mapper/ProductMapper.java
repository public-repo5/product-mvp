/**
 *
 */

package net.product.mapper;

import net.product.model.Product;
import net.product.model.ProductPostRequest;
import net.product.model.ProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author namho
 * 
 */
@Mapper
public interface ProductMapper {

  ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);
  Product toProduct(ProductPostRequest product);
  ProductResponse toProductPostResponse(Product product);
}
