/**
 *
 */

package net.product.service;

import net.product.repo.ProductRepo;
import net.product.dto.ProductListRequest;
import net.product.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static net.product.utils.Constants.INIT_PAGE;
import static net.product.utils.Constants.MAX_PAGE_SIZE;

/**
 * @author namho
 * 
 */
@Service
public class ProductService {

    private ProductRepo productRepo;

    public ProductService(@Autowired ProductRepo productRepo) {
        this.productRepo = productRepo;
    }


    public Optional<Product> findById(final Long id) {
        return productRepo.findById(id);
    }

    @Transactional
    public Product createProduct(final Product product) {
        final Product savedProduct = productRepo.save(product);
        return savedProduct;
    }

    public Page<Product> listProduct(final ProductListRequest productRequest) {
        Specification spec = (Specification<Product>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (productRequest.getCategory() != null) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("category"), productRequest.getCategory())));
            }
            if (productRequest.getBrand() != null) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("brand"), productRequest.getBrand())));
            }
            if (productRequest.getColor() != null) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("color"), productRequest.getColor())));
            }
            if (productRequest.getFromPrice() != null) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("currentPrice"), productRequest.getFromPrice())));
            }
            if (productRequest.getToPrice() != null) {
                predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("currentPrice"), productRequest.getToPrice())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
        int page = INIT_PAGE;
        int size = MAX_PAGE_SIZE;
        if (productRequest.getLimit()!= null && productRequest.getLimit() < size) {
            size = productRequest.getLimit();
        }
        if (productRequest.getOffset() != null && productRequest.getOffset() > 0) {
            page = productRequest.getOffset() / size;
        }
        return productRepo.findAll(spec, PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "id")));
    }
}
