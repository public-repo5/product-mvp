package net.product.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;


@Getter
@AllArgsConstructor
public class ProductListRequest {
    private String category;
    private BigDecimal fromPrice;
    private BigDecimal toPrice;
    private String brand;
    private String color;
    private Integer offset;
    private Integer limit;
}
