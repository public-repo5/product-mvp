/**
 *
 */

package net.product.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author namho
 * 
 */
@Slf4j
public class IncomingRequestUserContextFilter extends OncePerRequestFilter {

  public IncomingRequestUserContextFilter() {
  }

  @Override
  protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
    final FilterChain filterChain) throws ServletException, IOException {
    try {
      final UserContextHolder.UserContext userContext = UserContextHolder.getContext();
      userContext.setUserId(request.getHeader("X-User-Id"));
      filterChain.doFilter(request, response);
    } finally {
      UserContextHolder.getContext().clear();
      logger.debug("UserContextHolder now cleared, as request processing completed");
    }

  }
}
