/**
 *
 */

package net.product.utils;

import lombok.Data;
import org.springframework.util.Assert;

/**
 * @author namho
 * 
 */
public class UserContextHolder {

  private static final ThreadLocal<UserContext> userContext = new ThreadLocal<>();

  private UserContextHolder() {
  }

  public static final UserContext getContext() {
    UserContext context = (UserContext) userContext.get();
    if (context == null) {
      context = createEmptyContext();
      userContext.set(context);
    }

    return (UserContext) userContext.get();
  }

  public static final void setContext(final UserContext context) {
    Assert.notNull(context, "Only non-null UserContext instances are permitted");
    userContext.set(context);
  }

  public static final UserContext createEmptyContext() {
    return new UserContext();
  }

  @Data
  public static class UserContext {

    private String userId;

    public void clear() {
      this.userId = null;
    }
  }

}
