/**
 *
 */

package net.product.model;

import lombok.Getter;
import lombok.Setter;
import net.product.utils.UserContextHolder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author namho
 * 
 */
@Getter
@Setter
@Entity
@Table(name = "PRODUCT")
@AttributeOverride(name = "id", column = @Column(name = "ID", insertable = false, updatable = false))
@GenericGenerator(name = "SEQ_GEN", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
  parameters = { @Parameter(name = "sequence_name", value = "SEQ_PRODUCT") })
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;

  @Column(name = "category")
  private String category;

  @Column(name = "brand")
  private String brand;

  @Column(name = "color")
  private String color;

  @Column(name = "currentPrice")
  private BigDecimal currentPrice;

  @Column(name = "source")
  private String source;

  @Column(name = "imageURL")
  private String imageURL;

  @Column(name = "createdBy")
  private String createdBy;

  @Column(name = "createdAt")
  private Long createdAt;

  @Column(name = "updatedBy")
  private String updatedBy;

  @Column(name = "updatedAt")
  private Long updatedAt;

  @PrePersist
  public void onCreateProduct() {
    this.createdBy = UserContextHolder.getContext().getUserId();
    this.createdAt = System.currentTimeMillis();
  }

  @PreUpdate
  public void onUpdateProduct() {
    this.updatedBy = UserContextHolder.getContext().getUserId();
    this.updatedAt = System.currentTimeMillis();
  }
}
