/**
 *
 */

package net.product.repo;

import net.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author namho
 * 
 */
@Repository
public interface ProductRepo extends JpaRepository<Product, Long> , JpaSpecificationExecutor<Product> {
}
