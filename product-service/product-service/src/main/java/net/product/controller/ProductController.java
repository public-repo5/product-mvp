/**
 *
 */

package net.product.controller;

import lombok.extern.slf4j.Slf4j;
import net.product.dto.ProductListRequest;
import net.product.mapper.ProductMapper;
import net.product.model.Product;
import net.product.model.ProductListResponse;
import net.product.model.ProductPostRequest;
import net.product.model.ProductResponse;
import net.product.service.ProductService;
import org.openapitools.api.ProductApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.util.stream.Collectors;

/**
 * @author namho
 * 
 */
@Controller
@Slf4j
public class ProductController implements ProductApi {
    private ProductService productService;

    public ProductController(@Autowired ProductService productService) {
        this.productService = productService;
    }

    @Override
    public ResponseEntity<ProductListResponse> listProducts(String category, BigDecimal fromPrice, BigDecimal toPrice, String brand, String color, Integer offset, Integer limit) {
        Page<Product> page = productService.listProduct(new ProductListRequest(category, fromPrice, toPrice, brand, color, offset, limit));
        ProductListResponse response = new ProductListResponse();
        response.setTotal(page.getTotalElements());
        response.setProducts(page.getContent().stream().map(product -> ProductMapper.INSTANCE
                .toProductPostResponse(product)).collect(Collectors.toList()));
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<ProductResponse> createProduct(ProductPostRequest productPostRequest) {
        return ResponseEntity.ok(ProductMapper.INSTANCE
                .toProductPostResponse(productService.createProduct(ProductMapper.INSTANCE.toProduct(productPostRequest))));
    }

    @Override
    public ResponseEntity<ProductResponse> getProduct(Long id) {
        return productService.findById(id).map(product -> ResponseEntity.ok(ProductMapper.INSTANCE
                .toProductPostResponse(product))).orElse(ResponseEntity.notFound().build());
    }
}
