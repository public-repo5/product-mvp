package net.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCircuitBreaker
@ComponentScan(basePackageClasses = { ProductServiceApplication.class })
@EntityScan(basePackageClasses = ProductServiceApplication.class)
public class ProductServiceApplication {

  public static void main(final String[] args) {
    SpringApplication.run(ProductServiceApplication.class, args);
  }
}
