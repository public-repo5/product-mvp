/**
 *
 */

package net.product.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.hibernate.StaleObjectStateException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.validation.ConstraintViolationException;
import java.nio.file.AccessDeniedException;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author namho
 * 
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @ExceptionHandler({ Throwable.class })
  public ResponseEntity handleException(final Throwable ex) {
    log.error("{}: {}", new Object[] { ex.getClass().getSimpleName(), ex.getMessage(), ex });
    HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
    final String errorMessage;
    if (ex instanceof EntityNotFoundException) {
      code = HttpStatus.NOT_FOUND;
      errorMessage = "Entity not found.";
    } else if (!(ex instanceof StaleObjectStateException) && !(ex instanceof OptimisticLockException)) {
      if (ex instanceof AuthenticationException) {
        code = HttpStatus.UNAUTHORIZED;
        errorMessage = "Your account is disabled, please contact the administrator.";
      } else if (ex instanceof AccessDeniedException) {
        code = HttpStatus.FORBIDDEN;
        errorMessage = "You do not have permission to use this action. Please contact the site administrator to request access.";
      } else if (!(ex instanceof MethodArgumentNotValidException)
        && !(ex instanceof MethodArgumentTypeMismatchException)) {
        if (ex instanceof ConstraintViolationException) {
          code = HttpStatus.BAD_REQUEST;
          final String errorFields = (String) ((ConstraintViolationException) ex).getConstraintViolations().stream()
            .map((cv) -> {
              if (Objects.isNull(cv)) {
                return "null";
              } else {
                final String path = cv.getPropertyPath().toString();
                return StringUtils.substringAfterLast(path, ".") + ": " + cv.getMessage();
              }
            }).collect(Collectors.joining(", "));
          errorMessage = "Error fields: " + errorFields;
        } else {
          errorMessage = "An unexpected exception occurred. Please try again or send a support request to administrators.";
        }
      } else {
        code = HttpStatus.BAD_REQUEST;
        errorMessage = "An unexpected mandatory attributes.";
      }
    } else {
      code = HttpStatus.CONFLICT;
      errorMessage = "The object has been updated or deleted by another user. Please reload the page and try again.";
    }
    return new ResponseEntity(errorMessage, code);
  }

}
